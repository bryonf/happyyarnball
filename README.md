Overview:

This file contains instructions for downloading the built Happy Yarnball game (source hosted at https://github.com/snelson3/Happy-Yarnball/releases/tag/final). This game was developed as a project for the CIS 410 Game Programming course at the University of Oregon, taught by Professor Eric Wills. The game was developed using the Unity engine by a team of five students including myself.

These files are meant to allow a user to play Windows, Mac OSX, and Linux standalone builds in their respective desktop environments to allow the game to be played the way it was meant to be played. You may also find the web player build at https://bryonf.bitbucket.org, but please note that the web player build was not initially a requirement during the beginning of this game's development (and is currently a request/suggestion) and so the game is less efficient when run in a browser and you may experience moments of severe lag or unresponsiveness as web player efficiency wasn't a consideration.




How to download and run the game:

1. Head to the source page (https://bitbucket.org/bryonf/happyyarnball/src).

1. Select a folder above corresponding to the operating system you're running.

2. If you're running a Windows or Linux operating system, determine whether you're using a 32-bit or 64-bit system.

	i. If running a 32-bit Windows or Linux system, you should download the corresponding \*_x86.zip or \*_x86.tgz file.

	ii. If running a 64-bit Windows or Linux system, you may download any one of the \*_x86.zip, \*_x86.tgz, \*_x86_64.zip, or \*_x86_64.tgz files.
	
	iii. If running a Mac OSX operating system, you should download the only available .zip file, which will work universally.

3. Once you have determined which file to download, you may either...

	i. Right-click the file and select an appropriate "Save As" option (e.g. in Windows, you will select "Save Link As...").

	ii. Click on the file, then click on the "Raw" button. You will be prompted to save the file.

4. After saving the file, you may need to extract the contents and select a place to store them.

5. Run the file as you would for any other application for your operating system.

	i. For Windows, you should simply go to your game folder and run the HappyYarnball_x86 or HappyYarnball_x86_64.exe file.

	ii. For Mac OSX, you should either install the .app file or navigate to the /Contents/MacOS/ folder in the .app file and run the HappyYarnball_OSX file directly.

	iii. For Linux, you should run the .x86 or .x86_64 file directly (you may need to add execute permissions to the .x86 or .x86_64 file contained in the folder).


	

Final notes:
It's possible that the instructions above won't be enough to get things running. As the person writing this document, I only have a Windows machine to test the downloads on and am unfamiliar with the process of installing or executing applications on an OSX or Linux system. I have had to rely on Google for including the downloadable builds and the sparse instructions I have provided, so you may need to do some of your own leg work to get things set up properly. That being said, these standalone builds should all work correctly right out of the box, so if you run into any unexpected issues, please reach out and I will do my best to provide a prompt and effective solution. I would also be more than appreciative to receive any feedback for making these instructions more comprehensive and useful.



Author: Bryon Fleming | bfleming@cs.uoregon.edu